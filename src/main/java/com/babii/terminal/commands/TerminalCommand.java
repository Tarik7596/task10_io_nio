package com.babii.terminal.commands;

public interface TerminalCommand{
	void execute(String[] args);
}
