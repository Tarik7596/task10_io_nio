package com.babii.terminal.commands;

public class ExitCommand implements TerminalCommand{

	@Override
	public void execute(String[] args) {
		System.exit(0);
	}

	@Override
	public String toString() {
		return "[Closing terminal] example close";
	}
	
}
