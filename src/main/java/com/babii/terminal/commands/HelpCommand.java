package com.babii.terminal.commands;

import com.babii.terminal.managers.TerminalManager;

public class HelpCommand implements TerminalCommand{
	private TerminalManager manager;

	public HelpCommand(TerminalManager manager) {
		this.manager = manager;
	}

	@Override
	public void execute(String[] args){
		manager.printCommands();
	}
	@Override
	public String toString() {
		return "[Displays available commands] example help";
	}
}
