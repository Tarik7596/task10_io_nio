package com.babii.terminal.commands;

import com.babii.terminal.managers.FileManager;

import java.io.IOException;

public class CDCommand implements TerminalCommand{
	private FileManager manager;

	public CDCommand(FileManager manager) {
		this.manager = manager;
	}

	public void execute(String[] args){
		try{
			if(args.length == 2)
			{
				if(args[1].contains(":"))
					manager.setBrandNewDirectory(args[1]);
				else
					manager.setSubDirectory(args[1]);
			}
			else{
				System.out.println("You did not specify directory");
			}
		}catch(IOException e){
			System.out.println(e.getMessage());
		}
	}
	@Override
	public String toString() {
		return "[Set Directory] example cd D:\\lab)";
	}
}
