package com.babii.terminal.commands;


import com.babii.terminal.managers.FileManager;

public class DIRCommand implements TerminalCommand{
	private FileManager manager;

	public DIRCommand(FileManager manager) {
		this.manager = manager;
	}

	@Override
	public void execute(String[] args){
		if(args.length == 1)
			manager.displayDir();
	}
	@Override
	public String toString() {
		return "[Display nested files and folders] example dir";
	}

}
