package com.babii.terminal;

import com.babii.terminal.commands.CDCommand;
import com.babii.terminal.commands.DIRCommand;
import com.babii.terminal.commands.ExitCommand;
import com.babii.terminal.commands.HelpCommand;
import com.babii.terminal.managers.FileManager;
import com.babii.terminal.managers.TerminalManager;
import java.io.IOException;

public class App {
	public static void main(String[] args) throws IOException {
		FileManager fManager = new FileManager("C:");
		TerminalManager tManager = new TerminalManager();
		tManager.addCommand("dir", new DIRCommand(fManager));
		tManager.addCommand("cd", new CDCommand(fManager));
		tManager.addCommand("exit", new ExitCommand());
		tManager.addCommand("help", new HelpCommand(tManager));
		
		Terminal terminal = new Terminal(tManager, fManager);
		terminal.start();
	}
}
