package com.babii.terminal.managers;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileManager {
	private File file;

	public FileManager(String path) throws IOException {
		file = new File(path);
		if(!file.exists())
			throw new IOException("Specified directory is not exists");
	}
	public void setBrandNewDirectory(String newPath) throws IOException{
		File newFile = new File(newPath);
		if(!newFile.exists())
			throw new IOException("Specified directory is not exists");
		else
			file = newFile;
	}
	public void setSubDirectory(String subPath) throws IOException{
		File newFile = new File(file.getPath() + "\\" + subPath);
		if(!newFile.exists())
			throw new IOException("Specified directory is not exists");
		else
			file = newFile;
	}
	public void displayDir(){
		File[] files = file.listFiles();

		for(File file : files){
			System.out.printf("%-20s%-10s%-20s%-20s\n",  
					getFormattedDate(file.lastModified()),
					getFileType(file),
					getFormattedLength(file),
					file.getName());
		}
	}
	private String getFormattedLength(File file){
		if(file.isDirectory())
			return "";
		long fileLength = file.length();
		String result = "";
		int i = 0;
		while(fileLength > 0){
			if(i % 3 == 0)
				result += " ";
			result += fileLength % 10;
			fileLength /= 10;
			i++;
		}
		return new StringBuilder(result).reverse().toString();
	}
	private String getFileType(File file){
		if(file.isDirectory())
			return "<DIR>";
		else
			return "";
	}
	private String getFormattedDate(long miliss){
		Date d = new Date(miliss);
		SimpleDateFormat formatter = 
			      new SimpleDateFormat("dd.MM.yyyy hh:mm");
		return formatter.format(d);
	}
	public File getFile() {
		return file;
	}
	
}
