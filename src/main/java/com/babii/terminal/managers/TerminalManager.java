package com.babii.terminal.managers;

import com.babii.terminal.commands.TerminalCommand;
import java.util.LinkedHashMap;
import java.util.Map;

public class TerminalManager {
	private Map<String, TerminalCommand> commands =
			new LinkedHashMap<String, TerminalCommand>();
	
	public TerminalManager(){
	}
	
	public void runCommand(String[] args){
		if(commands.containsKey(args[0]))
			commands.get(args[0]).execute(args);
	}
	public void addCommand(String name, TerminalCommand command){
		commands.put(name, command);
	}
	public void printCommands(){
		for(String name : commands.keySet()){
			System.out.println(name + "\t" + commands.get(name));
		}
	}
	public Map<String, TerminalCommand> getCommands(){
		return commands;
	}
}

