package com.babii.terminal;

import com.babii.terminal.managers.FileManager;
import com.babii.terminal.managers.TerminalManager;
import java.io.IOException;
import java.util.Scanner;

public class Terminal {
	private TerminalManager tManager;
	private FileManager fManager;
	public static final Scanner scanner = new Scanner(System.in);
	
	public Terminal(TerminalManager tManager, FileManager fManager) throws IOException{
		this.tManager = tManager;
		this.fManager = fManager;
	}
	public void start(){
		System.out.println("Terminal v1.0 (type help to see available commands)");
		while(true){
			System.out.print(fManager.getFile().getPath() + "> ");
			String input = scanner.nextLine();
			String[] args = input.split(" ");
		
			tManager.runCommand(args);
		}
	}
}
