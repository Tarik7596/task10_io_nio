package com.babii.buffreadperformance;

import java.io.*;
import java.util.LinkedHashMap;
import java.util.Map;

public class Test {
    public static Map<Integer, Long[]> run(int fileSizeInBytes, int[] bufSizesInBytes){

        Map<Integer, Long[]> results = new LinkedHashMap<>();
        for(int bufSize : bufSizesInBytes){
            Long[] timesInMs = new Long[2];
            timesInMs[0] = testWriteFile(fileSizeInBytes, bufSize);
            timesInMs[1] = testReadFile(bufSize);
            results.put(bufSize, timesInMs);
        }
        return results;
    }
    public static long testWriteFile(int fileSizeInBytes, int bufSizeInbytes){
        String fileName = "test" + "(buffer=" + bufSizeInbytes/1024 + "KBytes).dat";
        File file = new File(fileName);
        byte[] buffer = new byte[32*1024]; // buffer has size of 32 KB

        long start = System.currentTimeMillis();
        try(BufferedOutputStream bos =
                    new BufferedOutputStream(new FileOutputStream(file), bufSizeInbytes)){
            while(file.length() < fileSizeInBytes)
                bos.write(buffer);
        }
        catch(IOException e){
            e.printStackTrace();
        }
        long end = System.currentTimeMillis();
        file.deleteOnExit();
        return end - start;
    }
    public static long testReadFile(int bufSizeInbytes){
        String fileName = "test" + "(buffer=" + bufSizeInbytes/1024 + "KBytes).dat";
        File file = new File(fileName);

        long start = System.currentTimeMillis();
        try(BufferedInputStream bis =
                    new BufferedInputStream(new FileInputStream(file), bufSizeInbytes)){
            int data = bis.read();
            while(data != -1)
                data = bis.read();
        }
        catch(IOException e){
            e.printStackTrace();
        }
        long end = System.currentTimeMillis();
        return end - start;
    }
}
