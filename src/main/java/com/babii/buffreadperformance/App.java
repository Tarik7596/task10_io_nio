package com.babii.buffreadperformance;

import java.util.Map;

public class App {
    public static void main(String[] args) {
        final int MB_TO_BYTE = 1024 * 1024;
        final int KB_TO_BYTE = 1024;
        final int BYTE_TO_KB = 1024;

        final int FILE_SIZE_IN_BYTES = 200 * MB_TO_BYTE;
        int[] bufSizesInBytes = {
                3 * KB_TO_BYTE,
                8 * KB_TO_BYTE,
                16 * KB_TO_BYTE,
                32 * KB_TO_BYTE,
                1 * MB_TO_BYTE,
                2 * MB_TO_BYTE,
                FILE_SIZE_IN_BYTES/8,
                FILE_SIZE_IN_BYTES/4,
                FILE_SIZE_IN_BYTES
        };


        Map<Integer, Long[]> results = Test.run(FILE_SIZE_IN_BYTES, bufSizesInBytes);
        System.out.println("Buffer size(KBytes)\tTime Write(ms)\tTime Read(ms)");
        System.out.println("-------------------------------------------------------------");
        for(Integer bufSize : results.keySet()){
            System.out.println(bufSize/BYTE_TO_KB + "\t\t\t"
                    + results.get(bufSize)[0] + "\t\t"
                    + results.get(bufSize)[1]);
        }
    }
}
