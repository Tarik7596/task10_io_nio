package com.babii.comments;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

public class CommentParser {
    private List<Comment> comments;

    private PushbackReader reader;

    public CommentParser(File file) throws FileNotFoundException {
        if (!file.exists())
            throw new FileNotFoundException("File is not exist");
        reader = new PushbackReader(new FileReader(file));
    }

    private void parse() throws IOException {
        boolean manyLinesOpened = false;
        boolean oneLineOpened = false;
        comments = new LinkedList<>();


        StringBuilder comment = new StringBuilder();

        while (true) {
            int symbol = reader.read();
            if (symbol == '/') {
                int next = reader.read();
                if (next == '/' && !manyLinesOpened && !oneLineOpened)
                    oneLineOpened = true;
                else if (next == '*' && !oneLineOpened && !manyLinesOpened) {
                    manyLinesOpened = true;
                } else{
                    reader.unread(next);
                    comment.append((char)symbol);
                }
            } else if (symbol == '\r') {
                //reader.skip(1);
                if (oneLineOpened) {
                    comments.add(new Comment(CommentType.ONELINE_COMMENT, comment.toString()));
                    oneLineOpened = false;
                    comment = new StringBuilder();
                }
            } else if (symbol == '*') {
                int next = reader.read();
                if (next == '/' && !oneLineOpened) {
                    comments.add(new Comment(CommentType.MANYLINES_COMMENT, comment.toString()));
                    manyLinesOpened = false;
                    comment = new StringBuilder();
                } else{
                    reader.unread(next);
                    comment.append((char)symbol);
                }
            }
            // if there is on \n in the last line of file
            else if(symbol == -1){
                if (oneLineOpened) {
                    comments.add(new Comment(CommentType.ONELINE_COMMENT, comment.toString()));
                    oneLineOpened = false;
                    comment = new StringBuilder();
                }
                break;
            }else if (manyLinesOpened || oneLineOpened) {
                comment.append((char) symbol);
            }
        }
    }

    public List<Comment> getComments() throws IOException {
        if(comments == null)
            parse();
        return comments;
    }
}
