package com.babii.comments;

public class Comment {
    private String data;
    private CommentType type;
    public Comment(CommentType type, String data){
        this.data = new String(data);
        this.type = type;
    }
    public String getData() {
        return data;
    }
    public CommentType getType() {
        return type;
    }
    public String getStringType() {
        if(type == CommentType.ONELINE_COMMENT)
            return "OneLine Comment";
        else
            return "ManyLines Comment";
    }
}
