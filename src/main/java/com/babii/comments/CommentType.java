package com.babii.comments;

public enum CommentType {
    ONELINE_COMMENT, MANYLINES_COMMENT
}
