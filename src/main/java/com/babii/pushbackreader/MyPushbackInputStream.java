package com.babii.pushbackreader;

import java.io.IOException;
import java.io.InputStream;

public class MyPushbackInputStream implements AutoCloseable {
    private InputStream is;
    private byte[] buf;
    private int pos;

    public MyPushbackInputStream(InputStream is) {
        this(is, 1);
    }

    public MyPushbackInputStream(InputStream is, int size){
        this.is = is;
        this.buf = new byte[size];
        this.pos = size;
    }
    public int read() throws IOException {
        if(pos < buf.length)
            return buf[pos++];
        return is.read();
    }
    public void unread(int data) throws IOException{
        if(pos == 0)
            throw new IOException("Buffer is full");
        buf[--pos] = (byte)data;
    }

    @Override
    public void close() throws IOException {
        if (is == null)
            return;
        is.close();
        is = null;
        buf = null;
    }
}
