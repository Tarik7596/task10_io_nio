package com.babii.pushbackreader;

import java.io.FileInputStream;
import java.io.IOException;

public class App {
    public static void main(String[] args){
        System.out.println("replacing all '4' in stream into '5' ");
        try (MyPushbackInputStream myStream = new MyPushbackInputStream(new FileInputStream("for_task4.txt"), 1)) {
            int data = myStream.read();
            while (data != -1) {
                if (data == (byte) '4') {
                    myStream.unread((byte) '5');
                    data = myStream.read();
                    continue;
                }
                System.out.print((char) data + " ");
                data = myStream.read();

            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
