package com.babii.comments;

import java.io.File;
import java.io.IOException;
import java.util.List;

/*
ManyLinesComment 1
*/
public class App {
    public static void main(String[] args) {
        run();
    }
    //oneline comment to method run()
    public static void run() {
        File file = new File("Source.java");

		/*many
		lines
		comment 2*/List<Comment> comments = null;
        try {
            CommentParser parser = new CommentParser(file);
            comments = parser.getComments();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        if (comments == null || comments.size() == 0) {
            System.out.println("No comments!");
        } else {
            for (Comment comment : comments) {
                System.out.println("------------");
                System.out.println(comment.getStringType() + ":\n" +  comment.getData());
            }

        }
    }
}